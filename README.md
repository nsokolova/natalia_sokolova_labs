This is public repo for all your labs. 
You should work in you own dev branches, they are never merged with master.
All commits on dev branches MUST be stable and consist only of approved code. 
Of course, you can create more branches and manage them as you wish. As a result some of them will be merged with your dev branch.

You MUST create initial commit in all branches. In feature branch I want to see more then one commit, before I approve your code.
I will approve only history of your development.
Code approval should be asked for in Issue tracker or as a pull request.